# foenig

An alias bash script for irrsi to fönify text before sending it to the channel. This is just a dirty hack, so please use with caution.

Inspiration from "Der Fönig" by Walter Mörs.
https://www.youtube.com/watch?v=TCDZG9OArvc

The content of config goes to ~/.irssi/config into the alias section.